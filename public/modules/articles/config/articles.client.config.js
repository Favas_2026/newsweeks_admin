'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Artículos', 'articles', 'dropdown', '/articles(/create)?');
		Menus.addSubMenuItem('topbar', 'articles', 'Lista de artículos', 'articles');
		Menus.addSubMenuItem('topbar', 'articles', 'Nuevo artículo', 'articles/create');
	}
]);