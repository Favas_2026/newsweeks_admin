'use strict';

module.exports = {
	app: {
		title: 'Newsweek Administrador en Español',
		description: 'Newsweek en Español ofrece una potente combinación de noticias, cultura e ideas, desde un punto de vista fresco - audaz, ágil y perspicaz.',
		keywords: 'Newsweek en Español ofrece una potente combinación de noticias, cultura e ideas, desde un punto de vista fresco - audaz, ágil y perspicaz.'
	},
	port: process.env.PORT || 4030,
	templateEngine: 'swig',
	sessionSecret: 'Newsweek',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
				'public/lib/css/font-awesome.min.css',
				'public/lib/css/ace-rtl.min.css',
				'public/lib/css/ace-skins.min.css',
				'public/lib/css/ace.min.css',
				'public/lib/css/css.css',
				//'public/lib/wysiwyg-html5/lib/css/bootstrap.min.css',
				'public/lib/wysiwyg-html5/lib/css/prettify.css',
				//'public/lib/wysiwyg-html5/src/bootstrap-wysihtml5.css'
				'public/lib/kendo/kendo.common.min.css',
				'public/lib/kendo/kendo.dataviz.min.css',
				'public/lib/kendo/kendo.metro.mobile.min.css',
				'public/lib/kendo/kendo.dataviz.metro.min.css'
			],
			js: [
				'public/lib/angular/angular.js',
				//'http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js',
				'public/lib/bootstrap-wysiwyg/jquery-1.11.1.min.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
				'public/lib/kendo/kendo.all.min.js',
				'public/lib/kendo/kendo.all.min.js',
				'public/lib/bootstrap-wysiwyg/scripts.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};